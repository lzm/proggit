// my entry for the proggit quiz challenge #9
// http://proggitquiz.com/challenge/9/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char **grid;
int x, y, n;

void alloc_grid()
{
	grid = calloc(x, sizeof(char*));
	for (int i=0; i<x; i++) {
		grid[i] = calloc(y, sizeof(char));
	}
}

void rand_grid()
{
	for (int i=0; i<x; i++) {
		for (int j=0; j<y; j++) {
			grid[i][j] = rand() % n;
		}
	}
}

void print_grid()
{
	for (int i=0; i<x; i++) {
		for (int j=0; j<y; j++) {
			printf("%d ", grid[i][j]);
		}
		puts("");
	}
}

int i_, j_, ii_, jj_;

int find_rect()
{
	for (int i=0; i<x-1; i++) {
		for (int j=0; j<y-1; j++) {
			for (int ii=i+1; ii<x; ii++) {
				if (grid[i][j] != grid[ii][j]) continue;
				for (int jj=j+1; jj<y; jj++) {
					if (grid[i][j] == grid[i][jj] &&
						grid[i][j] == grid[ii][jj])
					{
						i_ = i;
						j_ = j;
						ii_ = ii;
						jj_ = jj;
						return 1;
					}
				}
			}
		}
	}

	return 0;
}

int main(int argc, char *argv[])
{
	if (argc < 4) {
		printf("%s x y n\n", argv[0]);
		return -1;
	}

	x = atoi(argv[1]);
	y = atoi(argv[2]);
	n = atoi(argv[3]);

	srand(time(0));

	alloc_grid();
	rand_grid();

	while (find_rect()) {
		grid[i_][j_] = rand() % n;
		grid[i_][jj_] = rand() % n;
		grid[ii_][j_] = rand() % n;
		grid[ii_][jj_] = rand() % n;
	}

	print_grid();

	return 0;
}

