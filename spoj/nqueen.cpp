// http://www.spoj.pl/problems/NQUEEN/

#include <iostream>
#include <vector>

using namespace std;

int n, npos;
int orig[100];
int sol[100];
int pos[100];
int tmp[100];

int position_next(int p);

int check_valid(int p)
{
	int sp = sol[p];

	for (int i=0; i<n; i++)
	{
		int si = sol[i];

		if (si == 0 || i == p)
			continue;

		if (si == sp)
			return 0;

		int diff = i - p;

		if (si - diff == sp)
			return 0;

		if (si + diff == sp)
			return 0;
	}

	return 1;
}

void print_solution()
{
	cout << sol[0];
	for (int i=1; i<n; i++) {
		cout << " " << sol[i];
	}
	cout << endl;
}

int position_next(int p)
{
	for (;;) {
		if (p == n) {
			print_solution();
			return 1;
		}
		if (orig[p] == 0)
			break;
		p++;
	}

	npos--;

	for (int i=0; i<=npos; i++) {
		sol[p] = pos[i];
		pos[i] = pos[npos];

		if (check_valid(p))
			if (position_next(p+1))
				return 1;

		pos[i] = sol[p];
	}

	npos++;
	sol[p] = 0;

	return 0;
}

int main()
{
	while (cin >> n) {
		for (int i=1; i<=n; i++) {
			tmp[i] = 1;
		}
		for (int i=0; i<n; i++) {
			cin >> orig[i];
			sol[i] = orig[i];
			tmp[sol[i]] = 0;
		}
		npos = 0;
		for (int i=1; i<=n; i++) {
			if (tmp[i] == 1) {
				pos[npos++] = i;
			}
		}

		position_next(0);
	}

	return 0;
}
