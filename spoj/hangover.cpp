#include <iostream>

using namespace std;

int main()
{
	double f;
	for (;;) {
		cin >> f;
		if (f == 0.00) break;

		int n = 1;
		double o = 0.5;		

		while (o < f) {
			n++;
			o += 1.0/(n+1);
		}

		cout << n << " card(s)" << endl;
	}

	return 0;
}
