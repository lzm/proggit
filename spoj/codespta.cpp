#include <iostream>

using namespace std;

uint64_t powbits[33] = {0};

void init_powbits()
{
	for (int i=1; i<33; i++) {
		powbits[i] = (uint64_t)i << (i-1);
	}
}

int fls(uint32_t n)
{
	int ret = 0;
	while (n) {
		ret++;
		n >>= 1;
	}
	return ret;
}

uint64_t countbitu(uint32_t n)
{
	if (n == 0)
		return 0;

	int h = fls(n) - 1;
	int over = n - (1 << h);

	return powbits[h] + over + countbitu(over) + 1;
}

uint64_t countbits(int64_t n)
{
	if (n >= 0)
		return countbitu(n);
	
	int64_t nn = -n;

	return nn * 32 - countbitu(nn - 1);
}

uint64_t subbits(int32_t a, int32_t b)
{
	if (a == 0 || b == 0)
		return countbits(a) + countbits(b);

	if (a < 0 && b > 0)
		return countbits(a) + countbits(b);

	if (a < 0 && b < 0)
		return countbits(a) - countbits(b+1);

	if (a > 0 && b > 0)
		return countbits(b) - countbits(a-1);

	return 0;
}

int main()
{
	init_powbits();

	int t;
	cin >> t;
	
	while (t--) {
		int64_t a, b;
		cin >> a >> b;
		cout << subbits(a, b) << endl;
	}

	return 0;
}

