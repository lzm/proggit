import sys
print "".join([m[d] for m,d in zip([dict(zip("0123456789",ln.split())) for ln in ['Z M MM MMM','Z C CC CCC CD D DC DCC DCCC CM','Z X XX XXX XL L LX LXX LXXX XC','Z I II III IV V VI VII VIII IX']], "%04d" % int(sys.argv[1]))]).replace('Z', '')
