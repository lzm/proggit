%{
// roman.l by lzm
// flex roman.l && gcc lex.yy.c && echo "MCMXLIV" | ./a.out
int num = 0;
%}

%option noyywrap

%%
M+      num += yyleng*1000;
CM      num += 900;
D       num += 500;
CD      num += 400;
C+      num += yyleng*100;
XC      num += 90;
L       num += 50;
XL      num += 40;
X+      num += yyleng*10;
IX      num += 9;
V       num += 5;
IV      num += 4;
I+      num += yyleng;
%%

int main()
{
        yylex();
        printf("%d\n", num);
        return 0;
}
